import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';


import { LocalStorageProvider } from '../providers/local-storage/local-storage';
import { UserProvider } from '../providers/user/user';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  msg : any;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public localStorage: LocalStorageProvider,
    private userProvider : UserProvider,
    private alertCtrl : AlertController
  ) {


    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    localStorage.getRootPage().then((res : any) => {
      if(res.msg=='cookie-is-empty'){
        this.rootPage = LoginPage;
      }else if(res.user){
        try {
          userProvider.signin({
            email : res.user.email,
            password : res.user.password
          }).subscribe((data:any) => {
            if (data.msg==true) {
              this.rootPage = HomePage;
            }else if(data.err=='user-not-found'){
              const alertUserNotFound = this.alertCtrl.create({
                title: 'Kayıtlı Kullanıcı Bulunamadı',
                subTitle: 'Uygulamaya giriş yaptığınız kullanıcı sistemde bulunamadı! Lütfen tekrar giriş yapınız.',
                buttons: ['OK']
              });
              localStorage.deleteUserCookie().then((data : any )=>{
                if(data.msg){
                  console.log('user cookie deleted');
                }
              });
              alertUserNotFound.present();
              this.rootPage = LoginPage;
            }else if(data.err=='internal-server-error'){
              const alertInternalServerError = this.alertCtrl.create({
                title: 'Sistem Hatası',
                subTitle: 'Uygulama girişle ilgili bir sıkıntıyla karşılaştı. Lütfen bir daha deneyiniz!',
                buttons: ['OK']
              });
              alertInternalServerError.present();
            }
          });
        } catch (e) {
          const alertNetworkError = this.alertCtrl.create({
            title: 'Bağlantı Hatası',
            subTitle: 'Uygulama internete bağlanmakta sorun yaşıyor. Lütfen bağlantınızı kontrol edip uygulamayı tekrar açınız.',
            buttons: ['OK']
          });
          alertNetworkError.present();
        }

      }
    });
  }
}
