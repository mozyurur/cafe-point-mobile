import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { PopoverController } from 'ionic-angular';

import { HomeContentPage } from '../home-content/home-content';
import { ProfilePage } from '../profile/profile';
import { SettingsPage } from '../settings/settings';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  homeContentRoot;
  profileRoot;
  settingsRoot;

  constructor(public navCtrl: NavController,public popoverCtrl : PopoverController) {

    this.homeContentRoot = HomeContentPage;
    this.profileRoot = ProfilePage;
    this.settingsRoot = SettingsPage;

  }

}
