import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeContentPage } from './home-content';

@NgModule({
  declarations: [
    HomeContentPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeContentPage),
  ],
})
export class HomeContentPageModule {}
