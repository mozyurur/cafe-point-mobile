import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

/**
 * Generated class for the HomeContentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home-content',
  templateUrl: 'home-content.html',
})
export class HomeContentPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private localStorage : LocalStorageProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeContentPage');
    this.localStorage.getUserInfo().then((data : any) => {
      console.log(data);
    });
  }

}
