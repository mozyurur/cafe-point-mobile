import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { SignupPage } from '../signup/signup';
import { HomePage } from '../home/home';

import { UserProvider } from '../../providers/user/user';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

// import * as $ from 'jquery'

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  password; email;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams, private userProvider : UserProvider, private localStorage : LocalStorageProvider) {

  }

  navigateToSignupPage(){
    console.log('Clicked');
    this.navCtrl.push(SignupPage);
  }

  loginHttpRequest(){
    this.userProvider.signin({
      email : this.email,
      password : this.password
    }).subscribe((data:any) => {
      if (data.msg==true) {
        this.navCtrl.push(HomePage);
        this.localStorage.setUserCredentials({
          name : data.user.name,
          email : data.user.email,
          password : data.user.password
        }).then((data : any) => {
        });
      }else if(data.err=='user-not-found'){
        const alertUserNotFound = this.alertCtrl.create({
          title: 'Giriş İşlemi Başarısız',
          subTitle: 'Mail adresinizi ve ya şifrenizi yanlış girdiniz!',
          buttons: ['OK']
        });
        alertUserNotFound.present();
      }else if(data.err=='internal-server-error'){
        const alertInternalServerError = this.alertCtrl.create({
          title: 'Giriş İşlemi Başarısız',
          subTitle: 'Uygulama bir sıkıntıyla karşılaştı. Lütfen bir daha deneyiniz!',
          buttons: ['OK']
        });
        alertInternalServerError.present();
      }
    });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
