import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';


import { RequestServiceProvider } from '../../providers/request-service/request-service';

import { LoginPage } from '../login/login';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  name; username; password; password_control; email; type;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams, public httpClient: HttpClient, private requestService : RequestServiceProvider) {
  }

  signupHTTP(){
    this.requestService.signup({
      name: this.name,
      username: this.username,
      password: this.password,
      email: this.email,
      type: this.type
    }).subscribe((data:any) => {
      if (data._id) {
        this.navCtrl.push(LoginPage);
      }else {
        const alert = this.alertCtrl.create({
          title: 'Kayıt İşlemi Başarısız',
          subTitle: 'Girdiğiniz bilgiler kayıt işlemini başarısız kılmıştır.!',
          buttons: ['OK']
        });
        alert.present();
        console.log('Couldnt sign up');
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

}
