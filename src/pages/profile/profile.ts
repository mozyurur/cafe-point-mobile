import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { LocalStorageProvider } from '../../providers/local-storage/local-storage';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  name;email;

  constructor(public navCtrl: NavController, public navParams: NavParams, private localStorage : LocalStorageProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    this.localStorage.getUserInfo().then((data : any) => {
      this.name = data.user.name;
      this.email = data.user.email;
      console.log(data);
    });
  }

}
