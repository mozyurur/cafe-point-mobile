import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  constructor(public httpClient: HttpClient) {
    console.log('Hello UserProvider Provider');
  }

  signin(data){
    return this.httpClient.post('http://localhost:3002/user/signin', 'email='+data.email+'&password='+data.password, {
       headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    }).map(res => {
      return res;
    });
  }

}
