import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

/*
  Generated class for the RequestServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RequestServiceProvider {

  constructor(private httpClient: HttpClient) {
    console.log('Hello RequestServiceProvider Provider');
  }

  signup(data){
    return this.httpClient.post('http://localhost:3002/signup', 'username='+data.username+'&password='+data.password+'&name='+data.name+'&type='+data.type+'&email='+data.email, {
       headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    }).map(res => {
      return res;
    });
  }



}
