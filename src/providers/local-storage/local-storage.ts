import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage';

/*
  Generated class for the LocalStorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocalStorageProvider {

  constructor(public http: HttpClient, public storage: Storage) {
    console.log('Hello LocalStorageProvider Provider');
  }

  deleteUserCookie(){
    return new Promise(resolve => {
      this.storage.remove('user').then(()=>{
        resolve({
          'msg' : true
        });
      })
    });
  }

  getRootPage(){
    return new Promise(resolve => {
      this.storage.ready().then(() => {
        return this.storage.get('user').then((user)=>{
          if (user) {
            resolve({
              'user' : user
            });
          }else {
            resolve({
              'msg' : 'cookie-is-empty'
            });
          }
        })
      });
    });
  }

  setUserCredentials(data){
    return new Promise(resolve => {
      this.storage.set('user', {
        'name' : data.name,
        'email' : data.email,
        'password' : data.password

      }).then((user) => {
        resolve(user);
      });
    });
  }

  getUserInfo(){
    return new Promise(resolve => {
      this.storage.ready().then(() => {
        return this.storage.get('user').then((user)=>{
          if (user) {
            resolve({
              'user' : {
                'name' : user.name,
                'email' : user.email
              }
            });
          }else {
            resolve({
              'msg' : 'user-not-found'
            });
          }
        })
      });
    });
  }

}
